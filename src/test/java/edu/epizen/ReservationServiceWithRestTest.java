package edu.epizen;

import edu.epizen.core.aircraft.Aircraft;
import edu.epizen.core.flights.CapacityStatus;
import edu.epizen.core.flights.FlightDescription;
import edu.epizen.model.Reservation;
import edu.epizen.repository.ReservationRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@AutoConfigureMockMvc
@SpringBootTest
class ReservationServiceWithRestTest {

    @Autowired ReservationService reservationService;
    @MockBean ReservationRepository reservationRepository;
    @Autowired private MockMvc mvc;

    @BeforeEach
    public void setUp() {
        final Aircraft a = new Aircraft("BO74740072014CDGAFKLMP9E11",
                                            "BOEING 747-400 v7 2014",
                                        "AIR FRANCE KLM",
                                1500);
        final FlightDescription f = new FlightDescription("TO3530",a);
        final Reservation r = new Reservation("TAEZDFH", 20, f);
        final Optional<Reservation> o = Optional.of(r);

        final List<String> resa = new ArrayList<>(List.of(
                            "TAEZDFJ","TAEZDFK", "TAEZDFG", "TAEZDFP", "TAEZDFL",
                            "TAEZDFM", "TAEZDFN", "TAEZDFI", "TAEZDFO"));
        final List<Reservation> l = new ArrayList<>();
        for(final String s : resa) {
            final Reservation n =  new Reservation(s, 11, f);
            l.add(n);
        }
        l.add(r);

        Mockito.when(reservationRepository.findById("TAEZDFH")).thenReturn(o);
        Mockito.when(reservationRepository.findAll()).thenReturn(l);

    }

    @Test
    void validateLuggageUsingRestShouldReturnOK () throws Exception {

        mvc.perform(MockMvcRequestBuilders.
                    get("/epiflyzen/api/validate-lugguage").
                    contentType(MediaType.APPLICATION_JSON).
                    accept(MediaType.APPLICATION_JSON).
                    content("{\"flightNumber\" : \"TO3530\", \"reservationNumber\" : \"TAEZDFH\"}")).
                    andExpect(MockMvcResultMatchers.status().isOk()).
                    andExpect(MockMvcResultMatchers.content().json("{\"desc\":\"OK\",\"capacity\":119}"));
    }

}