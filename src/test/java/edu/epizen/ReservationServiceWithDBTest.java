package edu.epizen;

import edu.epizen.core.flights.CapacityStatus;
import edu.epizen.core.flights.FlightDescription;
import edu.epizen.model.Reservation;
import edu.epizen.repository.ReservationRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Map;

@SpringBootTest // (classes = {ReservationRepository.class, ReservationService.class, FlightsService.class})
class ReservationServiceWithDBTest {
    @Autowired ReservationService reservationService;
    @Test
    void validateLuggageWithDBShouldReturnOK() {
        final Map<Long, CapacityStatus> capacityStatusMap =
                    reservationService.validateLuggage("TAEZDFH", "TO3530");
        Assertions.assertEquals(200, capacityStatusMap.keySet().iterator().next());
        Assertions.assertEquals(CapacityStatus.OK, capacityStatusMap.values().iterator().next());
    }

}