package edu.epizen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "edu.epizen")
public class EpiflyzenApplication {
    public static void main(String... args) {
        SpringApplication.run(EpiflyzenApplication.class, args);
    }
}


