package edu.epizen;

import edu.epizen.core.flights.FlightDescription;
import edu.epizen.model.Reservation;
import org.springframework.stereotype.Service;

import java.util.LinkedHashSet;
import java.util.Set;

@Service
public class FlightsService {
    public Set<Reservation> getReservations(final FlightDescription flightDescription) {
        // Create a 10 foo reservations
        final LinkedHashSet<Reservation> reservations = new LinkedHashSet<>();
        for (int i = 0; i < 10; i++) {
            reservations.add(new Reservation());
        }
        return reservations;
    }
}
