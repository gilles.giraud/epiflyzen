package edu.epizen.model;

import edu.epizen.core.flights.FlightDescription;
import org.springframework.boot.autoconfigure.domain.EntityScan;

import javax.persistence.*;

@Entity
@Table(name = "reservations")
public class Reservation {
    @Id
    @Column(name = "reservation_number")
    private String reservationNumber;

    @Column(name = "reserved_lugguage")
    private long reservedLugguage;

    @ManyToOne
    @JoinColumn(name = "flight_number")
    private FlightDescription flightDescription;

    public Reservation() {
    }

    public Reservation(String reservationNumber, long reservedLugguage, FlightDescription flightDescription) {
        this.reservationNumber = reservationNumber;
        this.reservedLugguage = reservedLugguage;
        this.flightDescription = flightDescription;
    }

    public long getReservedLuggage() {
        return reservedLugguage;
        // return 10; // as for example
    }

    public void setReservedLugguage(long reservedLugguage) {
        this.reservedLugguage = reservedLugguage;
    }

    public void setReservationNumber(String reservationNumber) {
        this.reservationNumber = reservationNumber;
    }


    public String getReservationNumber() {
        return reservationNumber;
    }

    public FlightDescription getFlightDescription() {
        return flightDescription;
    }

    public void setFlightDescription(FlightDescription flightDescription) {
        this.flightDescription = flightDescription;
    }
}
