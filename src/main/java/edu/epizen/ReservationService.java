package edu.epizen;

import edu.epizen.core.aircraft.Aircraft;
import edu.epizen.core.flights.CapacityStatus;
import edu.epizen.core.flights.FlightDescription;
import edu.epizen.model.Reservation;
import edu.epizen.repository.AirCraftRepository;
import edu.epizen.repository.FlightRepository;
import edu.epizen.repository.ReservationRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Slf4j
@Service
public class ReservationService {

    @Autowired FlightsService flightsService;
    @Autowired ReservationRepository reservationRepository;
    @Autowired FlightRepository flightRepository;
    @Autowired AirCraftRepository airCraftRepository;


    public Map<Long,CapacityStatus> validateLuggage(final String reservationNUmber, final String flightNumber) {

        final Optional<Reservation> current = reservationRepository.findById(reservationNUmber);
        log.info("Current Reservation found : {}, check for {}",
                    current.get().getReservationNumber(),
                    current.get().getReservedLuggage());
        long maximumLuggageCapacity = 0;
         List<Reservation> reservations = (List<Reservation>) reservationRepository.findAll();

        long currentFlightLugguageCapacity = 0;
        for (final Reservation reservation : reservations) {
            if (current.get().getReservationNumber().equals(reservation.getReservationNumber())) {
                continue;
            }
            if (reservation.getFlightDescription().getFlightNumber().equals(flightNumber)) {
                log.info("Reservation : {} Reserved={} for Flight{} on a {})",
                        reservation.getReservationNumber(),
                        reservation.getReservedLuggage(),
                        reservation.getFlightDescription().getFlightNumber(),
                        reservation.getFlightDescription().getAirCraft().getModel());
                currentFlightLugguageCapacity += reservation.getReservedLuggage();
                // Same for all reservations.
                maximumLuggageCapacity = reservation.getFlightDescription().getAirCraft().getMaximumLuggageCapacity();
            }

        }
        log.info("Maximum = {}", maximumLuggageCapacity);
        final long currentCapacity = current.get().getReservedLuggage();
        final long computedFullCapacity = currentFlightLugguageCapacity + currentCapacity;
        final Map<Long, CapacityStatus> capacityStatusMap = new LinkedHashMap<>(1);
        if ( computedFullCapacity > maximumLuggageCapacity) {
            capacityStatusMap.put(computedFullCapacity,CapacityStatus.THRESHOLD_EXCEEDED);
        } else {
            capacityStatusMap.put(computedFullCapacity,CapacityStatus.OK);
        }
        return capacityStatusMap;
    }

    public Map<Long,CapacityStatus> validateLuggage__(final Reservation current, final FlightDescription flightDescription) {
        final Aircraft aircraft = flightDescription.getAirCraft();
        long maximumLuggageCapacity = aircraft.getMaximumLuggageCapacity();

        // Foo method
        Set<Reservation> reservations = flightsService.getReservations(flightDescription);

        long currentFlightLugguageCapacity = 0;
        for (final Reservation reservation : reservations) {
            currentFlightLugguageCapacity += reservation.getReservedLuggage();
        }
        final long currentCapacity = current.getReservedLuggage();
        final long computedFullCapacity = currentFlightLugguageCapacity + currentCapacity;
        final Map<Long, CapacityStatus> capacityStatusMap = new LinkedHashMap<>(1);
        if ( computedFullCapacity > maximumLuggageCapacity) {
            capacityStatusMap.put(computedFullCapacity,CapacityStatus.THRESHOLD_EXCEEDED);
        } else {
            capacityStatusMap.put(computedFullCapacity,CapacityStatus.OK);
        }
        return capacityStatusMap;
    }

}
