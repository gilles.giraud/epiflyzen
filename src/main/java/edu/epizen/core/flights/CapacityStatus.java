package edu.epizen.core.flights;

public enum CapacityStatus {
    OK("OK"), THRESHOLD_EXCEEDED("Threshold Exceeded");
    private  final String desc;

    CapacityStatus() {
        this.desc = "OK";
    }
    CapacityStatus(String desc) {
        this.desc = desc;
    }
    public String desc() {
        return desc;
    }
}
