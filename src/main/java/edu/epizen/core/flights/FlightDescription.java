package edu.epizen.core.flights;

import edu.epizen.core.aircraft.Aircraft;
import edu.epizen.core.airline.AirlineCompany;
import edu.epizen.core.airport.Airport;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "flights")
public class FlightDescription {
    @Id
    @Column(name = "flight_number")
    private String flightNumber;

//    private Date utcDepartTime;
//    private Date utcArrivalTime;
//    private AirlineCompany airlineCompany;
//    private Airport destination;
//    private Airport provenance;
    @ManyToOne
    @JoinColumn(name = "aircraft_id")
    private Aircraft airCraft;


    public  Aircraft getAirCraft() {
        return airCraft;
    }

    public FlightDescription() {
    }

    public FlightDescription(String flightNumber, Aircraft airCraft) {
        this.flightNumber = flightNumber;
        this.airCraft = airCraft;
    }

    public void setAirCraft(Aircraft airCraft) {
        this.airCraft = airCraft;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }
}
