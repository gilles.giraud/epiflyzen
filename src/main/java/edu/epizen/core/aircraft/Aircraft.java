package edu.epizen.core.aircraft;

import edu.epizen.core.flights.OperationnalFlight;
import edu.epizen.core.parking.ParkingSchedule;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "aircrafts")
public class Aircraft {
    @Id
    @Column(name = "aircraft_id")
    private String aircraftId;
    @Column(name = "model")
    private String model;
    @Column(name = "airline_company")
    private String airlineCompany;
    @Column(name = "maximum_lugguage_capacity")
    private int maximumLugguageCapacity;

    final Set<OperationnalFlight> getOperationnalFlights(final Date from, final Date to) {
        return null;
    }
    final Set<ParkingSchedule> getParkingScheduled(final Date from, final Date to) {
        return null;
    }

    public Aircraft(String aircraftId, String model, String airlineCompany, int maximumLugguageCapacity) {
        this.aircraftId = aircraftId;
        this.model = model;
        this.airlineCompany = airlineCompany;
        this.maximumLugguageCapacity = maximumLugguageCapacity;
    }

    public Aircraft() {
    }

    public String getAircraftId() {
        return aircraftId;
    }

    public void setAircraftId(String id) {
        this.aircraftId = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getAirlineCompany() {
        return airlineCompany;
    }

    public void setAirlineCompany(String airlineCompany) {
        this.airlineCompany = airlineCompany;
    }

    public long getMaximumLuggageCapacity() {
        //return 1500; // As for example
        return maximumLugguageCapacity;
    }

    @Override
    public String toString() {
        return "Aircraft{" +
                "id='" + aircraftId + '\'' +
                ", model='" + model + '\'' +
                ", airlineCompany='" + airlineCompany + '\'' +
                ", maximumLugguageCapacity=" + maximumLugguageCapacity +
                '}';
    }
}



