package edu.epizen.core.parking;

import edu.epizen.core.aircraft.Aircraft;

import java.util.Date;

public class ParkingSchedule {
    private ParkingArea parkingArea;
    private Aircraft aircraft;
    private Date from;
    private Date upto;

    public ParkingArea getParkingArea() {
        return parkingArea;
    }
}
