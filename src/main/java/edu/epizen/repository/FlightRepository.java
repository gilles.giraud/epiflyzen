package edu.epizen.repository;

import edu.epizen.core.flights.FlightDescription;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FlightRepository extends CrudRepository<FlightDescription, String> {

}

