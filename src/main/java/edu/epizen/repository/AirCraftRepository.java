package edu.epizen.repository;

import edu.epizen.core.aircraft.Aircraft;
import org.springframework.data.repository.CrudRepository;

public interface AirCraftRepository extends CrudRepository<Aircraft, String> {
}
