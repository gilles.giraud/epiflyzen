package edu.epizen.endpoints;

import edu.epizen.core.flights.CapacityStatus;
import edu.epizen.core.flights.FlightDescription;
import edu.epizen.model.Reservation;
import edu.epizen.endpoints.dto.LugguageValidation;
import edu.epizen.endpoints.dto.LugguageValidationRequest;
import edu.epizen.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;


//curl -X GET http://localhost:46460/epiflyzen/api/validate-lugguage \
//        -H 'Content-Type: application/json' \
//        -H 'accept: application/json' \
//        -d @lugguage.json
//{
//        "flightNumber" : "TO3530",
//        "reservationNumber" : "TAEZDFH"
//        }


@RestController
@RequestMapping("/epiflyzen/api/")
public class EpiflyzenApiEndpoint {
    @Autowired ReservationService reservationService;
    @GetMapping("/validate-lugguage")
    public LugguageValidation validateLugguage(@RequestBody LugguageValidationRequest request) {
        final Map<Long, CapacityStatus> capacityStatusMap =
                reservationService.validateLuggage(request.getReservationNumber(), request.getFlightNumber());
        return new LugguageValidation(  capacityStatusMap.values().iterator().next().desc(),
                                        capacityStatusMap.keySet().iterator().next() );
    }
}
