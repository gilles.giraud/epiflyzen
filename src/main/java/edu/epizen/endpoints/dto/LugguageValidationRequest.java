package edu.epizen.endpoints.dto;

public class LugguageValidationRequest {
    private String flightNumber;
    private String reservationNumber;

    public LugguageValidationRequest() {
    }

    public LugguageValidationRequest(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getReservationNumber() {
        return reservationNumber;
    }

    public void setReservationNumber(String reservationNumber) {
        this.reservationNumber = reservationNumber;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }
}
