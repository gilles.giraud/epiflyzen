package edu.epizen.endpoints.dto;

public class LugguageValidation {
    private String desc;
    private long capacity;

    public LugguageValidation() {
    }

    public LugguageValidation(String desc, long capacity) {
        this.desc = desc;
        this.capacity = capacity;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public long getCapacity() {
        return capacity;
    }

    public void setCapacity(long capacity) {
        this.capacity = capacity;
    }
}
